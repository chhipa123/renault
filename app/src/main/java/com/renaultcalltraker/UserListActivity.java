package com.renaultcalltraker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.health.android.libprogressbar.CustomProgress;
import com.renaultcalltraker.Util.NetworkDetector;
import com.renaultcalltraker.Util.ServerConnection;
import com.renaultcalltraker.constant.Constant;
import com.renaultcalltraker.model.User;
import com.rets.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UserListActivity extends Activity {

    private String TAG = getClass().getName();
    public  ListView listView;
    public  ArrayList<User> userArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        init();
    }

    private void init() {
        userArrayList.clear();
        listView = (ListView) findViewById(R.id.listView);
        getTaskList();
    }

    private void getTaskList() {
        if (new NetworkDetector(UserListActivity.this).isConnectingToInternet()) {
            new GetUserAsyncTask(UserListActivity.this, Constant.URL+"userlist").execute("");
        } else {
            Toast.makeText(UserListActivity.this, Constant.NETWORK_ERROR_MESSAGE, Toast.LENGTH_LONG).show();
        }
    }


    class GetUserAsyncTask extends AsyncTask<String, Void, String> {
        private String TAG = "GetUserAsyncTask";
        private CustomProgress customProgress;
        private String url;
        private Context context;



        public GetUserAsyncTask(Context context, String url) {
            this.url = url;
            this.context = context;


        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            customProgress = CustomProgress.show(context, "", false, false, null);
        }

        @Override
        protected String doInBackground(String... params) {
            String res;
            try {
                res = new ServerConnection().makePostRequest2(url);
            } catch (Exception e) {
                res = null;
                e.printStackTrace();
            }

            return res;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            customProgress.dismiss();
            Log.i(TAG, "URL :Response  " + result);
            if (result != null) {
                parseServerResponse(result);
            } else {
                Toast.makeText(UserListActivity.this, Constant.NETWORK_FAIL_MSG, Toast.LENGTH_LONG).show();
            }
        }
    }



    private void parseServerResponse(String result) {

        Log.e(TAG, "Got server response " + result);
        if (result != null) {
            String status;

            try {
                JSONObject jsonObject = new JSONObject(result);
                status = jsonObject.getString("status");
                if (status.equalsIgnoreCase("Success")) {

                    JSONArray resultJson =  jsonObject.getJSONArray("result");

                    if(resultJson!= null){

                        for(int i=0;i<resultJson.length();i++){
                            JSONObject json =  resultJson.getJSONObject(i);

                            String id =  json.getString("id");
                            String uniq_id =  json.getString("uniq_id");
                            String auth_key =  json.getString("auth_key");
                            String name =  json.getString("name");
                            String contact =  json.getString("contact");
                            String email =  json.getString("email");
                            String lati =  json.getString("lat");
                            String longi =  json.getString("long");
//                        String times =  json.getString("times");
//                        String timescustom =  json.getString("times_custom");
//                        String created_time =  json.getString("created_time");
                        String update_time =  json.getString("update_time");
//                        String registerStatus =  json.getString("status");

                            User user = new User(id,uniq_id,auth_key,name,contact,email,lati,longi,update_time);
                            userArrayList.add(user);
                        }

                        listView.setAdapter(new ListAdapter(UserListActivity.this));
                        ((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();
                    }
                } else if (status.equalsIgnoreCase("Failed.")) {

                    if(listView!=null && listView.getAdapter() != null) {
                        ((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();
                    }
                    //Toast.makeText(UserListActivity.this, jsonObject.getString("msg"), Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(UserListActivity.this, Constant.NETWORK_FAIL_MSG, Toast.LENGTH_LONG).show();
                }

            } catch (JSONException e) {

                Log.i(TAG, "JSONException while parsing response :" + e);

            }

        }


    }


    class ListAdapter extends BaseAdapter {
        private Activity activity;
        private LayoutInflater inflater = null;

        public ListAdapter(Activity a) {
            activity = a;
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {
            return userArrayList.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            View rowView = null;
            Holder holder = null;
            if (convertView == null) {
                rowView = inflater.inflate(R.layout.user_list_cell, null);
                holder = new Holder();
                holder.userName = (TextView) rowView.findViewById(R.id.userName);
                holder.contact = (TextView) rowView.findViewById(R.id.contact);
                holder.email = (TextView) rowView.findViewById(R.id.email);
                holder.viewOnMap = (TextView) rowView.findViewById(R.id.viewOnMap);
                holder.callLog = (TextView) rowView.findViewById(R.id.callLog);
                rowView.setTag(holder);
            } else {
                rowView = convertView;
                holder = (Holder) rowView.getTag();
            }
            holder.userName.setText(userArrayList.get(position).getUserName());
            holder.contact.setText(userArrayList.get(position).getContact());
            holder.email.setText(userArrayList.get(position).getEmail());
            holder.viewOnMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(UserListActivity.this, MapScreenActivity.class);
                    intent.putExtra("latitude", userArrayList.get(position).getLatitude());
                    intent.putExtra("longitude", userArrayList.get(position).getLongitude());
                    intent.putExtra("username", userArrayList.get(position).getUserName());
                    intent.putExtra("contact", userArrayList.get(position).getContact());
                    intent.putExtra("email", userArrayList.get(position).getEmail());
                    intent.putExtra("time", userArrayList.get(position).getUpdatetime());
                    intent.putExtra("user_id", userArrayList.get(position).getId());
                    intent.putExtra("uniq_id", userArrayList.get(position).getUniq_id());
                    intent.putExtra("auth_key", userArrayList.get(position).getAuth_key());
                    startActivity(intent);

                }
            });
            holder.callLog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(UserListActivity.this, CallDetailActivity.class);
                    intent.putExtra("id", userArrayList.get(position).getId());
                    intent.putExtra("uniq_id", userArrayList.get(position).getUniq_id());
                    intent.putExtra("auth_key", userArrayList.get(position).getAuth_key());
                    Log.i("call detail","name"+userArrayList.get(position).getUserName());
                    startActivity(intent);
                }
            });

            return rowView;
        }

        private class Holder {
            public TextView userName,contact,email,viewOnMap,callLog;


        }
    }
}
