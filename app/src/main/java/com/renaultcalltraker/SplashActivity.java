package com.renaultcalltraker;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;

import com.renaultcalltraker.Util.UserPreference;
import com.renaultcalltraker.Util.Utility;
import com.rets.R;

import java.text.SimpleDateFormat;
import java.util.Date;


public class SplashActivity extends Activity {
    UserPreference session;
    Context mcontext;
    private Thread mThread;
    private boolean isFinish = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mcontext = this;


        session = new UserPreference(mcontext);
        mThread = new Thread(mRunnable);
        mThread.start();



    }

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            SystemClock.sleep(1000);
            mHandler.sendEmptyMessage(0);
        }
    };

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 0 && (!isFinish)) {
                stop();
            }
            super.handleMessage(msg);
        }

    };

    @Override
    protected void onDestroy() {
        try {
            mThread.interrupt();
            mThread = null;
        } catch (Exception e) {
        }
        super.onDestroy();
    }

    @SuppressLint("SimpleDateFormat")
    private void stop() {
        isFinish = true;


        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss aaa");
        String timeString = formatter.format(new Date(Integer.parseInt("1453446891") * 1000L));

        Log.i("timeString","timeString "+timeString);

        if (session.isLoginDone()) {

            if(Utility.getSharedPreferences(mcontext, "admin").equalsIgnoreCase("1") ){
                Intent intent = new Intent(mcontext, UserListActivity.class);
                mcontext.startActivity(intent);
                finish();

            }
//            else{
//                Intent i= new Intent(mcontext, MainActivity.class);
//                i.putExtra("checkPlayService",true);
//                mcontext.startService(i);
//                finish();
//            }


        }else{

            Intent intent = new Intent(mcontext, LoginActivity.class);
            mcontext.startActivity(intent);
            finish();
        }
    }


}
