package com.renaultcalltraker;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.renaultcalltraker.Util.CallLogUtil;
import com.renaultcalltraker.Util.MyDatabaseHandler;
import com.renaultcalltraker.Util.NetworkDetector;
import com.renaultcalltraker.Util.ServerConnection;
import com.renaultcalltraker.constant.Constant;

import org.json.JSONException;
import org.json.JSONObject;

public class SendMyCallLog {

    private MyDatabaseHandler database;

    public SendMyCallLog(Context context) {
        sendCallDetail(context);
    }


    public void sendCallDetail(Context context) {

        database = new MyDatabaseHandler(context);
        if (database.ifTableContainsRow("calllog")) {

            JSONObject jsonObject = CallLogUtil.getCallDetails(context);
            if (new NetworkDetector(context).isConnectingToInternet()) {
                new SendDataAsyncTask(Constant.URL + "addCallDetail", jsonObject, context).execute("");
            }

        }else{
            Log.i("","In sufficent call detail");
        }
    }

    public class SendDataAsyncTask extends AsyncTask<String, Void, String> {
        private String TAG = "SendDataAsyncTask";
        private String url;
        private JSONObject object;
        private Context context;

        public SendDataAsyncTask(String url, JSONObject object,Context context) {
            this.url = url;
            this.object = object;
            this.context = context;
            Log.i("","call detail url"+ url);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String res;
            try {
                res = new ServerConnection().makePostRequest(url, object);
            } catch (Exception e) {
                res = null;
                e.printStackTrace();
            }

            return res;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.i(TAG, "URL :Response  " + result);

            // Employee not exist
            if (result != null) {
                String status;
                try {

                    JSONObject jsonObject = new JSONObject(result);
                    status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("Success")) {

                        if(context != null){
                            database = new MyDatabaseHandler(context);

                            if(database.ifTableContainsRow("calllog")){
                                database.deleteAllRows("calllog");
                            }
                        }

                    } else if (status.equalsIgnoreCase("Failed")) {
                        if (jsonObject.getString("msg").equalsIgnoreCase("Employee not exist")) {

                        }
                    }
                } catch (JSONException e) {

                }finally {
                    if(database!=null){
                        database.close();
                    }
                }
            }
        }
    }
}


