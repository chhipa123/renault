package com.renaultcalltraker;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.health.android.libprogressbar.CustomProgress;
import com.renaultcalltraker.Util.NetworkDetector;
import com.renaultcalltraker.Util.ServerConnection;
import com.renaultcalltraker.constant.Constant;
import com.renaultcalltraker.model.CallDetail;
import com.rets.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class CallDetailActivity extends Activity {

    private DatePickerDialog datePickerDialog,datePickerDialog1;
    public ListView listView;
    public ArrayList<CallDetail> callArrayList = new ArrayList<>();
    public long startDate,endDate;

    String id,uniq_id,auth_key;
    SimpleDateFormat format1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_detail);

        format1 = new SimpleDateFormat("dd-MM-yyyy");
        id =  getIntent().getExtras().getString("id");
        uniq_id =  getIntent().getExtras().getString("uniq_id");
        auth_key =  getIntent().getExtras().getString("auth_key");
        listView = (ListView) findViewById(R.id.listViewCallDetail);
        final TextView firstDate = (TextView)findViewById(R.id.firstDate);
        firstDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();

            }
        });
        final TextView secoundDate = (TextView)findViewById(R.id.secoundDate);
        secoundDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                datePickerDialog1.show();
            }
        });
        TextView done = (TextView)findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getCallDetail();
            }
        });



        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DAY_OF_YEAR, -7);
        final Date daysBeforeDate = cal.getTime();
        firstDate.setText(format1.format(daysBeforeDate));
        startDate = daysBeforeDate.getTime();

        final Calendar newCalendar = Calendar.getInstance();
        secoundDate.setText(format1.format(newCalendar.getTime()));
        endDate = newCalendar.getTime().getTime();

        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth,12,0,0);
                firstDate.setText(format1.format(newDate.getTime()));
                startDate = newDate.getTime().getTime();
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog1 = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth,12,0,0);
                secoundDate.setText(format1.format(newDate.getTime()));
                endDate = newDate.getTime().getTime();
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        getCallDetail();

        LinearLayout linLayBack = (LinearLayout) findViewById(R.id.linLayBack);
        linLayBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
    }




    private void getCallDetail() {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("uniq_id", uniq_id);
            jsonObject.put("user_id", id);
            jsonObject.put("auth_key", auth_key);
            jsonObject.put("start_date", TimeUnit.MILLISECONDS.toSeconds(startDate));
            jsonObject.put("end_date", TimeUnit.MILLISECONDS.toSeconds(endDate));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (new NetworkDetector(CallDetailActivity.this).isConnectingToInternet()) {
            new GetcallDetailAsyncTask(CallDetailActivity.this, Constant.URL+"filterdCalls",jsonObject).execute("");
        } else {
            Toast.makeText(CallDetailActivity.this, Constant.NETWORK_ERROR_MESSAGE, Toast.LENGTH_LONG).show();
        }
    }


    class GetcallDetailAsyncTask extends AsyncTask<String, Void, String> {
        private String TAG = "GetUserAsyncTask";
        private CustomProgress customProgress;
        private String url;
        private Context context;
        JSONObject object;

        public GetcallDetailAsyncTask(Context context, String url,JSONObject object) {
            this.url = url;
            this.context = context;
            this.object = object;

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            customProgress = CustomProgress.show(context, "", false, false, null);
        }

        @Override
        protected String doInBackground(String... params) {
            String res;
            try {
                res = new ServerConnection().makePostRequest(url, object);
            } catch (Exception e) {
                res = null;
                e.printStackTrace();
            }

            return res;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            customProgress.dismiss();
            Log.i(TAG, "URL :Response  " + result);
            if (result != null) {
                parseServerResponse(result);
            } else {
                Toast.makeText(CallDetailActivity.this, Constant.NETWORK_FAIL_MSG, Toast.LENGTH_LONG).show();
            }
        }
    }



    private void parseServerResponse(String result) {

            String status;
            try {
                JSONObject jsonObject = new JSONObject(result);
                status = jsonObject.getString("status");
                callArrayList.clear();
                if (status.equalsIgnoreCase("Success")) {
                    JSONArray resultJson =  jsonObject.getJSONArray("data");
                    String checkDate = "";
                    if(resultJson!= null){
                        for(int i=0;i<resultJson.length();i++){
                            JSONObject json =  resultJson.getJSONObject(i);

                            String number =  json.getString("phNumber");
                            String type =  json.getString("callType");
                            String date =  json.getString("callDate");
                            String duration =  json.getString("callDuration");
                            CallDetail callDetail = new CallDetail(number.trim(),type,date,duration);

                            if(checkDate.isEmpty()) {
                                checkDate = format1.format(Integer.parseInt(date) * 1000L);
                                callDetail.setDateFlag(true);
                                callDetail.setCommonDate(checkDate);
                            }else{
                                if(checkDate.equalsIgnoreCase(format1.format(Integer.parseInt(date)* 1000L))){

                                }else{
                                    checkDate = format1.format(Integer.parseInt(date)* 1000L);
                                    callDetail.setDateFlag(true);
                                    callDetail.setCommonDate(checkDate);
                                }
                            }
                            callArrayList.add(callDetail);
                        }

                        listView.setAdapter(new ListAdapter(CallDetailActivity.this));

                    }
                } else if (status.equalsIgnoreCase("Failed")) {

                   // Toast.makeText(CallDetailActivity.this, jsonObject.getString("msg"), Toast.LENGTH_LONG).show();
                    if(listView!=null && listView.getAdapter() != null) {
                        ((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();
                    }
                }
            } catch (JSONException e) {

            }
    }




    class ListAdapter extends BaseAdapter {
        private Activity activity;
        private LayoutInflater inflater = null;


        public ListAdapter(Activity a) {
            activity = a;
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {
            return callArrayList.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            View rowView = null;
            Holder holder = null;
            if (convertView == null) {
                rowView = inflater.inflate(R.layout.call_list_cell, null);
                holder = new Holder();

                holder.partition = (TextView) rowView.findViewById(R.id.partition);
                holder.phoneNumber = (TextView) rowView.findViewById(R.id.phoneNumber);
                holder.date = (TextView) rowView.findViewById(R.id.date);
                holder.type = (TextView) rowView.findViewById(R.id.type);
                holder.duration = (TextView) rowView.findViewById(R.id.duration);

                rowView.setTag(holder);
            } else {
                rowView = convertView;
                holder = (Holder) rowView.getTag();
            }
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss aaa");
            SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");


            if (callArrayList.get(position).isDateFlag()) {
                holder.partition.setVisibility(View.VISIBLE);
                holder.partition.setText(callArrayList.get(position).getCommonDate());
                holder.phoneNumber.setText(callArrayList.get(position).getNumber());
                String dateString = formatter.format(new Date(Integer.parseInt(callArrayList.get(position).getDate()) * 1000L));
                holder.date.setText(dateString);
                holder.type.setText(callArrayList.get(position).getType());

                TimeZone tz = TimeZone.getTimeZone("UTC");
                df.setTimeZone(tz);
                if(Integer.parseInt(callArrayList.get(position).getDuration()) == 0){
                    holder.duration.setText("00:00:00");
                }else {
                    String time = df.format(new Date(Integer.parseInt(callArrayList.get(position).getDuration()) * 1000L));
                    holder.duration.setText(time);
                }
            } else {
                holder.partition.setVisibility(View.GONE);
                holder.phoneNumber.setText(callArrayList.get(position).getNumber());
                String dateString = formatter.format(new Date(Integer.parseInt(callArrayList.get(position).getDate()) * 1000L));
                holder.date.setText(dateString);
                holder.type.setText(callArrayList.get(position).getType());
                TimeZone tz = TimeZone.getTimeZone("UTC");
                df.setTimeZone(tz);
                if(Integer.parseInt(callArrayList.get(position).getDuration()) == 0){
                    holder.duration.setText("00:00:00");
                }else {
                String time = df.format(new Date(Integer.parseInt(callArrayList.get(position).getDuration()) * 1000L));
                holder.duration.setText(time);
                }
            }

            return rowView;
        }

        private class Holder {
            public TextView phoneNumber,date,type,duration,partition;

        }
    }
}
