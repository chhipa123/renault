package com.renaultcalltraker;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.health.android.libprogressbar.CustomProgress;
import com.renaultcalltraker.Util.NetworkDetector;
import com.renaultcalltraker.Util.ServerConnection;
import com.renaultcalltraker.Util.UserPreference;
import com.renaultcalltraker.Util.Utility;
import com.renaultcalltraker.constant.Constant;
import com.rets.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class LoginActivity extends Activity {

    private EditText employeeId;
    private Button signInButton;
    final String TAG = "LoginActivity";
    boolean checkPlayService;
    // flag for GPS status
    boolean isGPSEnabled = false;
    protected LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        checkPlayService = Utility.checkPlayServices(LoginActivity.this);


        locationManager = (LocationManager) LoginActivity.this
                .getSystemService(LOCATION_SERVICE);
        // getting GPS status
        isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        init();



        if (isGPSEnabled) {

        } else {
            showSettingsAlert();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

    }

    public void showSettingsAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(LoginActivity.this);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
                dialog.cancel();
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    private void init(){
        employeeId= (EditText) findViewById(R.id.edtEmployeeId);
        employeeId.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_GO) {

                    performLoginAction();
                    return true;
                }
                return false;
            }
        });
        signInButton= (Button) findViewById(R.id.btnLogin);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isGPSEnabled = locationManager
                        .isProviderEnabled(LocationManager.GPS_PROVIDER);
                if(isGPSEnabled){
                    performLoginAction();
                }else{
                    Toast.makeText(LoginActivity.this,"Turn on GPS before login",Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private void performLoginAction() {
        employeeId.setError(null);


        if (TextUtils.isEmpty(employeeId.getText().toString().trim())) {
            employeeId.setError(getString(R.string.LOGIN_USERNAME_REQUIRED));
            employeeId.requestFocus();
            return;
        }

        else {
            if(checkPlayService){
            if (new NetworkDetector(LoginActivity.this).isConnectingToInternet()) {
                new UserLoginAsyncTask(LoginActivity.this, Constant.URL+"login", setJSONRequest()).execute("");
            } else {
                Toast.makeText(LoginActivity.this, Constant.NETWORK_ERROR_MESSAGE, Toast.LENGTH_LONG).show();
            }
            }else{
                Toast.makeText(LoginActivity.this, "", Toast.LENGTH_LONG).show();
            }
        }
    }




    private JSONObject setJSONRequest() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uniq_id", "" + employeeId.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


    class UserLoginAsyncTask extends AsyncTask<String, Void, String> {
        private String TAG = "UserLoginAsyncTask";
        private CustomProgress customProgress;
        private String url;
        private JSONObject object;
        private Context context;



        public UserLoginAsyncTask(Context context, String url, JSONObject object) {
            this.url = url;
            this.context = context;
            this.object = object;

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            customProgress = CustomProgress.show(context, "", false, false, null);
        }

        @Override
        protected String doInBackground(String... params) {
            String res;
            try {
                res = new ServerConnection().makePostRequest(url, object);
            } catch (Exception e) {
                res = null;
                e.printStackTrace();
            }

            return res;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            customProgress.dismiss();
            Log.i(TAG, "URL :Response  " + result);
            if (result != null) {
                parseServerResponse(result);
            } else {
                Toast.makeText(LoginActivity.this, Constant.NETWORK_FAIL_MSG, Toast.LENGTH_LONG).show();
            }
        }
    }


    private void parseServerResponse(String result) {

        Log.e(TAG, "Got server response " + result);
        if (result != null) {
            String status;

            try {
                JSONObject jsonObject = new JSONObject(result);
                status = jsonObject.getString("status");
                if (status.equalsIgnoreCase("Success")) {

                    JSONObject resultJson =  jsonObject.getJSONObject("result");

                    if(resultJson!= null){

                        Utility.setSharedPreference(LoginActivity.this, "uniq_id", resultJson.getString("uniq_id"));
                        Utility.setSharedPreference(LoginActivity.this, "user_id" , resultJson.getString("id"));
                        Utility.setSharedPreference(LoginActivity.this, "auth_key", resultJson.getString("auth_key"));
                        Utility.setSharedPreference(LoginActivity.this, "admin", resultJson.getString("admin"));


                        if(resultJson.getString("admin").equalsIgnoreCase("1")){

                            UserPreference userPreference = new UserPreference(LoginActivity.this);
                            userPreference.saveLoginDone(resultJson.getString("id"));
                            startActivity(new Intent(LoginActivity.this, UserListActivity.class));
                        }else{
                            UserPreference userPreference = new UserPreference(LoginActivity.this);
                            userPreference.saveLoginDone(resultJson.getString("id"));
                            setAlarmForLtLng();
                           // setAlarmCallDetail();
                            if (new NetworkDetector(LoginActivity.this).isConnectingToInternet()) {

                                new SendMyLocation(LoginActivity.this,checkPlayService);
                                //new SendMyCallLog(LoginActivity.this);
                            }

                            PackageManager p = getPackageManager();
                            ComponentName componentName = new ComponentName(this, com.renaultcalltraker.SplashActivity.class); // activity which is first time open in manifiest file which is declare as <category android:name="android.intent.category.LAUNCHER" />
                            p.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);



//                            PackageManager p = getPackageManager();
//                            ComponentName componentName = new ComponentName(this, com.apps.MainActivity.class);
//                            p.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
                        }
                        finish();
                    }

                } else if (status.equalsIgnoreCase("Failed.")) {

                    Toast.makeText(LoginActivity.this, jsonObject.getString("msg"), Toast.LENGTH_LONG).show();
                    employeeId.setText("");
                } else {
                    Toast.makeText(LoginActivity.this, Constant.NETWORK_FAIL_MSG, Toast.LENGTH_LONG).show();
                    employeeId.setText("");
                }

            } catch (JSONException e) {
                Log.i(TAG, "JSONException while parsing response :" + e);
            }

        }


    }


    private void setAlarmForLtLng() {

        Date date = new Date();   // given date
        Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
        calendar.setTime(date);   // assigns calendar to given date

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent alarmIntent = new Intent(LoginActivity.this, AlarmReceiverForLocation.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmIntent.setData((Uri.parse("custom://" + System.currentTimeMillis())));
        alarmManager.cancel(pendingIntent);

        Calendar alarmStartTime = Calendar.getInstance();
        Calendar now = Calendar.getInstance();
        alarmStartTime.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY));
        alarmStartTime.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE)+1);
        alarmStartTime.set(Calendar.SECOND, 0);
        if (now.after(alarmStartTime)) {
            alarmStartTime.add(Calendar.DATE, 1);
        }
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmStartTime.getTimeInMillis(), Constant.ALARM_INTERVAL, pendingIntent);
    }


    private void setAlarmCallDetail() {
        Date date = new Date();   // given date
        Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
        calendar.setTime(date);   // assigns calendar to given date

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent alarmIntent = new Intent(LoginActivity.this, AlarmReceiver.class); // AlarmReceiver1 = broadcast receiver
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmIntent.setData((Uri.parse("custom://" + System.currentTimeMillis())));
        alarmManager.cancel(pendingIntent);
        Calendar alarmStartTime = Calendar.getInstance();
        Calendar now = Calendar.getInstance();
        alarmStartTime.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY));
        alarmStartTime.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE)+5);
        alarmStartTime.set(Calendar.SECOND, 0);
        if (now.after(alarmStartTime)) {
            alarmStartTime.add(Calendar.DATE, 1);
        }

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmStartTime.getTimeInMillis(), Constant.ALARM_INTERVAL_FOR_CALL, pendingIntent);
    }

}
