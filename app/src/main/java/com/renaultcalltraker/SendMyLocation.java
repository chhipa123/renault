package com.renaultcalltraker;


import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.renaultcalltraker.Util.ServerConnection;
import com.renaultcalltraker.Util.Utility;
import com.renaultcalltraker.constant.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

public class SendMyLocation implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private String TAG = "SendMyLocation";
    private String url;
    private Context context;
    private GoogleApiClient mGoogleApiClient;
    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 10000; // 10 sec
    private static int FATEST_INTERVAL = 5000; // 5 sec
    private static int DISPLACEMENT = 10; // 10 meters
    private LocationRequest mLocationRequest;
    private boolean checkPlayService;
    private Location mLastLocation;
    double  latitude ;
    double longitude ;
    long time ;


    public SendMyLocation(Context context,boolean checkPlayService) {
        this.context = context;
        this.checkPlayService = checkPlayService;
        if (checkPlayService) {
            // Building the GoogleApi client
            buildGoogleApiClient();
            createLocationRequest();
        }
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }


    /**
     * Creating google api client object
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }


    /**
     * Creating location request object
     */
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    class SendLatLngAsyncTask extends AsyncTask<String, Void, String> {
        private String TAG = "SendLatLngAsyncTask";
        private String url;
        private JSONObject object;

        public SendLatLngAsyncTask(String url, JSONObject object) {
            this.url = url;
            this.object = object;
            Log.i("","latlng url "+ url);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String res;
            try {
                res = new ServerConnection().makePostRequest(url, object);
            } catch (Exception e) {
                res = null;
                e.printStackTrace();
            }

            return res;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.i(TAG, "URL :Response  " + result);
            if (result != null) {
                String status;
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    status = jsonObject.getString("status");
                    if (status.equalsIgnoreCase("Success")) {

                       // Toast.makeText(context.getApplicationContext(),""+latitude+" "+longitude,Toast.LENGTH_LONG).show();

                    } else if (status.equalsIgnoreCase("Failed")) {

                    }
                } catch (JSONException e) {

                }

            }
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        // Once connected with google api, get the location
        mLastLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
             latitude = mLastLocation.getLatitude();
             longitude = mLastLocation.getLongitude();
             time = mLastLocation.getTime();

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("uniq_id", Utility.getSharedPreferences(context, "uniq_id"));
                jsonObject.put("auth_key", Utility.getSharedPreferences(context, "auth_key"));
                jsonObject.put("lat", "" + latitude);
                jsonObject.put("long", "" + longitude);
                jsonObject.put("times", "" + TimeUnit.MILLISECONDS.toSeconds(time));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.i("location log", "" + jsonObject.toString());
            new SendLatLngAsyncTask(Constant.URL + "updateUser",jsonObject).execute("");

        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());
    }

    @Override
    public void onLocationChanged(Location location) {
        // Assign the new location
        mLastLocation = location;

//        Toast.makeText(getApplicationContext(), "Location changed!",
//                Toast.LENGTH_SHORT).show();
//
//        // Displaying the new location on UI
//        displayLocation();


    }




}
