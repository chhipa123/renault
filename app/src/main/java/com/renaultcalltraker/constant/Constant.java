/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.renaultcalltraker.constant;

public final class Constant {


    public static final String URL = "http://103.76.253.132/emptrack/api/";

    public static final String NETWORK_ERROR_MESSAGE = "Internet Connection not available.";
    public static final String NETWORK_FAIL_MSG = "Not able to connect. Please check your network connection and try again.";
    public static final String CASE_URL = "http://cloudia.co.in/cases";
   // public static final long ALARM_INTERVAL = 240000;
    public static final long ALARM_INTERVAL = 15000;
    public static final long ALARM_INTERVAL_FOR_CALL = 300000;

}
