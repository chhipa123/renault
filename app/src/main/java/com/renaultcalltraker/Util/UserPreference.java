package com.renaultcalltraker.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class UserPreference {

	private static final String PREF_NAME = "MCOMAP_APP_AUTH";
	private static final String IS_LOGIN_DONE = "isLoginDone";
	private static final String USER_FIRST_NAME = "firstName";
	private static final String USER_LAST_NAME = "lastName";
	private static final String USER_EMAIL_NAME = "email";

	private static Editor editor;
	private SharedPreferences pref;

	public UserPreference(Context _activity) {
		pref = _activity.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		editor = pref.edit();
	}

	public void saveLoginDone(String auth) {
		editor.putString(IS_LOGIN_DONE, auth);
		editor.commit();
	}

	public boolean isLoginDone() {
		boolean auth = false;
		String value = pref.getString(IS_LOGIN_DONE, null);
		if (value != null) {
			auth = true;

		}

		return auth;
	}

	public String getLoginAuth() {
		return pref.getString(IS_LOGIN_DONE, "abc");
	}

	public void logoutDone() {
		editor.putString(IS_LOGIN_DONE, null);
		editor.commit();
	}

	public String getUserFirstName() {
		return pref.getString(USER_FIRST_NAME, "");
	}

	public void setUserFirstName(String s) {

		editor.putString(USER_FIRST_NAME, s);
		editor.commit();
	}

	public String getUserLastName() {
		return pref.getString(USER_LAST_NAME, "");
	}

	public void setUserLastName(String s) {
		editor.putString(USER_LAST_NAME, s);
		editor.commit();
	}

	public String getUserEmailName() {
		return pref.getString(USER_EMAIL_NAME, "");
	}

	public void setUserEmailName(String s) {
		editor.putString(USER_EMAIL_NAME, s);
		editor.commit();
	}

}
