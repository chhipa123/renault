package com.renaultcalltraker.Util;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ServerConnection {
    private static int timeoutSocket = 50000;
    private static int timeoutConnection = 50000;
    private String TAG = getClass().getName();




    public String makePostRequest(String targetURL, JSONObject jsonRequest)
            throws Exception {
        Log.i(TAG, "Request Json:-" + jsonRequest);

        String result = null;
        URL url;
        HttpURLConnection connection = null;
        try {
            //Create connection
            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setConnectTimeout(timeoutConnection);
            connection.setReadTimeout(timeoutSocket);
            connection.setRequestProperty("Content-Type",
                    "application/json");/*application/x-www-form-urlencoded*/
            connection.setRequestProperty("Accept", "application/json");

           /* connection.setRequestProperty("Content-Length", "" +
                    Integer.toString(urlParameters.getBytes().length));*/
           // connection.setRequestProperty("Content-Language", "en-US");
//            if(auth!=null){
//                connection.setRequestProperty("authToken",auth);
//                connection.setRequestProperty("accesscode",authCode);
//
//            }
//            connection.setUseCaches(false);
//            connection.setDoInput(true);
//            connection.setDoOutput(true);
            //Send request


            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(jsonRequest.toString());
            wr.flush();
            wr.close();
            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                // response.append('\r');
            }
            rd.close();
            result = response.toString();

        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "Message " + e.getMessage());
            Log.i(TAG,"LocalizedMessage "+e.getLocalizedMessage());
            Log.i(TAG,"Exception Cause "+e.getCause());
            Log.i(TAG,"StackTrace "+e.getStackTrace().toString());
            return null;

        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return result;
    }



    public String makePostRequest2(String targetURL)
            throws Exception {


        String result = null;
        URL url;
        HttpURLConnection connection = null;
        try {
            //Create connection
            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setConnectTimeout(timeoutConnection);
            connection.setReadTimeout(timeoutSocket);
            connection.setRequestProperty("Content-Type",
                    "application/json");/*application/x-www-form-urlencoded*/
            connection.setRequestProperty("Accept", "application/json");

           /* connection.setRequestProperty("Content-Length", "" +
                    Integer.toString(urlParameters.getBytes().length));*/
            // connection.setRequestProperty("Content-Language", "en-US");
//            if(auth!=null){
//                connection.setRequestProperty("authToken",auth);
//                connection.setRequestProperty("accesscode",authCode);
//
//            }
//            connection.setUseCaches(false);
//            connection.setDoInput(true);
//            connection.setDoOutput(true);
            //Send request
//            DataOutputStream wr = new DataOutputStream(
//                    connection.getOutputStream());
//            wr.writeBytes(jsonRequest.toString());
//            wr.flush();
//            wr.close();
            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                // response.append('\r');
            }
            rd.close();
            result = response.toString();

        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "Message " + e.getMessage());
            Log.i(TAG,"LocalizedMessage "+e.getLocalizedMessage());
            Log.i(TAG,"Exception Cause "+e.getCause());
            Log.i(TAG,"StackTrace "+e.getStackTrace().toString());
            return null;

        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return result;
    }



    public String makePostRequest1(String path, JSONObject object)
            throws Exception {
        Log.i(TAG, "Request JSON-" + object);
        HttpResponse response;
        InputStream content = null;
        String result = null;

        HttpPost httpost = new HttpPost(path);

        HttpParams httpParams = new BasicHttpParams();

//        httpost.setConnectionRequest("Content-Type",
//                "application/json");


        ConnManagerParams.setTimeout(httpParams, timeoutSocket);
        HttpConnectionParams
                .setConnectionTimeout(httpParams, timeoutConnection);
        HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket);

        HttpClient httpclient = new DefaultHttpClient(httpParams);

        StringEntity se = new StringEntity("datakey",object.toString());

        httpost.setEntity(se);

        //StringEntity se = new StringEntity(holder.toString());
     // httpost.setEntity(new UrlEncodedFormEntity(nameValuePair, "UTF-8"));


        //httpost.setEntity(se);

        //httpost.setHeader("Accept", "application/json");

        //httpost.setHeader("Content-type", "application/json");
        // httpost.setHeader("authToken", "saikat.ganguly@lmsin.comsaikat321");
        try {
            StringBuilder builder = new StringBuilder();
            response = httpclient.execute(httpost);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            Log.i("Connection", "statusCode..." + statusCode);
            //Constants.STATUS_CODE = statusCode;
            // if (statusCode == 200) {
            HttpEntity entity = response.getEntity();

            content = entity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    content));

            String line;
            while ((line = reader.readLine()) != null) {

                builder.append(line);

            }
            result = builder.toString();
            // } else {

            // Log.e(LoginActivity.class.toString(),
            // "Failed to download file");
            // }
        } catch (ClientProtocolException e) {
            // showSettingsAlert("" + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            // showSettingsAlert("" + e.getMessage());
            e.printStackTrace();
        }
        return result;
    }


//    public String makeGetRequest(String targetURL, String auth, String authCode)
//            throws Exception {
//        Log.i(TAG, "Request url:-" + targetURL);
//        Log.i(TAG, "Request auth:-" + auth);
//        String result = null;
//        URL url;
//        HttpURLConnection connection = null;
//        try {
//            //Create connection
//            url = new URL(targetURL);
//            connection = (HttpURLConnection) url.openConnection();
//            connection.setRequestMethod("GET");
//            connection.setConnectTimeout(timeoutConnection);
//            connection.setReadTimeout(timeoutSocket);
//            connection.setRequestProperty("Content-Type",
//                    "application/json");
//            connection.setRequestProperty("Accept", "application/json");
//           // connection.setRequestProperty("Content-Language", "en-US");
//            if(auth!=null){
//                connection.setRequestProperty("authToken",auth);
//                connection.setRequestProperty("accesscode",authCode);
//
//            }
//           // connection.setUseCaches(false);
//           // connection.setDoInput(true);
//            //connection.setDoOutput(true);
//
//            //Get Response
//            InputStream is = connection.getInputStream();
//            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
//            String line;
//            StringBuffer response = new StringBuffer();
//            while ((line = rd.readLine()) != null) {
//                response.append(line);
//                // response.append('\r');
//            }
//            rd.close();
//            result = response.toString();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.i(TAG,"Message " + e.getMessage());
//            Log.i(TAG,"LocalizedMessage "+e.getLocalizedMessage());
//            Log.i(TAG,"Exception Cause "+e.getCause());
//            Log.i(TAG,"StackTrace "+e.getStackTrace().toString());
//            return null;
//
//        } finally {
//
//            if (connection != null) {
//                connection.disconnect();
//            }
//        }
//        return result;
//    }



    public String makeGetRequest(String targetURL,Context mcontext)
            throws Exception {


        String result = null;
        URL url;
        HttpURLConnection connection = null;
        try {
            //Create connection
            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(timeoutConnection);
            connection.setReadTimeout(timeoutSocket);
            connection.setRequestProperty("Content-Type",
                    "application/json");
            connection.setRequestProperty("Accept", "application/json");

            byte[] auth = ("dhirajsingh@lmsin.com" + ":" + "12345").getBytes();
            String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
            connection.setRequestProperty("Authorization", "Basic " + basic);

//            byte[] auth = ("25" + ":" + "2eec876cb420d115763cbc0e3949cdd5").getBytes();
//            String basic = Base64.encodeToString(auth, Base64.NO_WRAP);
//            connection.setRequestProperty("Authorization", "Basic " + basic);
            // connection.setRequestProperty("Content-Language", "en-US");
//            if(auth!=null){
//                connection.setRequestProperty("authToken",auth);
//                connection.setRequestProperty("accesscode",authCode);
//
//            }
            // connection.setUseCaches(false);
            // connection.setDoInput(true);
            //connection.setDoOutput(true);



            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                // response.append('\r');
            }
            rd.close();
            result = response.toString();

        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG,"Message " + e.getMessage());
            Log.i(TAG,"LocalizedMessage "+e.getLocalizedMessage());
            Log.i(TAG,"Exception Cause "+e.getCause());
            Log.i(TAG,"StackTrace "+e.getStackTrace().toString());
            return null;

        } finally {

            if (connection != null) {
                connection.disconnect();
            }
        }
        return result;
    }









}
