package com.renaultcalltraker.Util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.renaultcalltraker.model.CallDetail;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class MyDatabaseHandler extends SQLiteOpenHelper {

	static String DATABASE_NAME = "renaultDB";
	static int VERSION = 1;

	public MyDatabaseHandler(Context context) {

		super(context, DATABASE_NAME, null, VERSION);

	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL("create table IF NOT EXISTS calllog(phNumber TEXT,callType TEXT,callDate TEXT UNIQUE ON CONFLICT IGNORE,callDuration TEXT)");

	}

	public void addCallDetail(CallDetail callDetail) {

		//if(!isRecordAvailable("calllog",callDetail.getDate())){

			//Log.i("databasehandler","addCallDetail");
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues value = new ContentValues();
		value.put("phNumber", callDetail.getNumber());
		value.put("callType", callDetail.getType());
		value.put("callDate", callDetail.getDate());
		value.put("callDuration", callDetail.getDuration());

		long inserted = db.insert("calllog", null, value);

//		Log.i("addCallDetail", "inserted " + inserted);
//		Log.d("addCallDetail", "inserted " + inserted);
//		Log.e("addCallDetail", "inserted " + ifTableContainsRow("calllog"));
//		ArrayList<CallDetail> callDetailTest =  showAllCallDetail();
//
//		for(int i=0; i<callDetailTest.size();i++) {
//
//			Log.d("addCallDetail", "inserted " + callDetailTest.get(i).getNumber()+" "+callDetailTest.get(i).getType());
//
//		}


		//}

	}

	public boolean isRecordAvailable(String tableName,String callDate) {

		if(!ifTableContainsRow(tableName)){
			return false;
		}

		Cursor cursor = getWritableDatabase().rawQuery(
				"SELECT * FROM " + tableName +" where callDate="+callDate, null);
				if (cursor.getCount() > 0) {
			return true;
		} else {
			return false;
		}
	}


	public ArrayList<CallDetail> showAllCallDetail() {
		ArrayList<CallDetail> list = new ArrayList<CallDetail>();
		CallDetail callDetail = null;
		SQLiteDatabase db = this.getReadableDatabase();
		//Cursor cur = db.query("calllog", null, null, null, null, null, null);
		Cursor cur = getWritableDatabase().rawQuery("select phNumber,callType,callDate,callDuration from calllog", null);

		while (cur.moveToNext()) {
			callDetail = new CallDetail(cur.getString(0), cur.getString(1), cur.getString(2),cur.getString(3));

			list.add(callDetail);

		}

		return list;

	}

//	public boolean searchFileIdWise(String value) {
//		FileModel fileModel = null;
//		SQLiteDatabase db = this.getReadableDatabase();
//		Cursor cur = db.query("downloadedfile", null, "fId=?",new String[] { value }, null, null, null);
//		boolean found = cur.moveToNext();
//		return found;
//
//	}

	public boolean ifTableContainsRow(String tableName) {
		Cursor cursor = getWritableDatabase().rawQuery(
				"SELECT count(*) FROM " + tableName, null);
		if (cursor.isBeforeFirst()) {
			cursor.moveToNext();
		}
		if (cursor.getInt(0) > 0) {
			return true;
		} else {
			return false;
		}
	}

	public int deleteAllRows(String tableName) {
		return getWritableDatabase().delete(tableName, null, null);
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {

	}

}
