package com.renaultcalltraker.Util;


import android.content.Context;
import android.util.Log;

import com.renaultcalltraker.model.CallDetail;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

//

public class CallLogUtil {

    static String TAG = "CallLogUtil";
    static MyDatabaseHandler database;

    public static JSONObject getCallDetails(Context mcontext) {

        database = new MyDatabaseHandler(mcontext);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uniq_id", Utility.getSharedPreferences(mcontext, "uniq_id"));
            jsonObject.put("user_id", Utility.getSharedPreferences(mcontext, "user_id"));
            jsonObject.put("auth_key", Utility.getSharedPreferences(mcontext, "auth_key"));
            long currentTime = getTodayTimestamp();
            jsonObject.put("date", TimeUnit.MILLISECONDS.toSeconds(currentTime));


                JSONArray jsArray = new JSONArray();
                ArrayList<CallDetail> callDetail =  database.showAllCallDetail();

                for(int i=0; i<callDetail.size();i++) {

                    JSONObject callLogJson = new JSONObject();
                    callLogJson.put("phNumber", callDetail.get(i).getNumber());
                    callLogJson.put("callType", callDetail.get(i).getType());
                    callLogJson.put("callDate", TimeUnit.MILLISECONDS.toSeconds(Long.valueOf(callDetail.get(i).getDate())));
                    callLogJson.put("callDuration", Long.valueOf(callDetail.get(i).getDuration()));
                    jsArray.put(callLogJson);

                }

                Log.i("jsArray log", "" + jsArray.toString());
                Log.i("Call log", "" + jsonObject.toString());
                jsonObject.put("data", jsArray);


            // managedCursor.close();
        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            if(database!=null){
                database.close();
            }
        }
        return jsonObject;

    }



    public static long getTodayTimestamp() {
        Calendar c1 = Calendar.getInstance();
        c1.setTime(new Date());

        Calendar c2 = Calendar.getInstance();
        c2.set(Calendar.YEAR, c1.get(Calendar.YEAR));
        c2.set(Calendar.MONTH, c1.get(Calendar.MONTH));
        c2.set(Calendar.DAY_OF_MONTH, c1.get(Calendar.DAY_OF_MONTH));
        c2.set(Calendar.HOUR_OF_DAY, 0);
        c2.set(Calendar.MINUTE, 0);
        c2.set(Calendar.SECOND, 0);

        return c2.getTimeInMillis();
    }

}
