package com.renaultcalltraker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CallLog;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.renaultcalltraker.Util.MyDatabaseHandler;
import com.renaultcalltraker.model.CallDetail;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class PhoneStateBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "Receiver";
    Context mContext;
    String incoming_nr;
    private int prev_state;
    MyDatabaseHandler database;



    @Override
    public void onReceive(Context context, Intent intent) {
        TelephonyManager telephony = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE); //TelephonyManager object
        CustomPhoneStateListener customPhoneListener = new CustomPhoneStateListener();
        telephony.listen(customPhoneListener, PhoneStateListener.LISTEN_CALL_STATE); //Register our listener with TelephonyManager

        Bundle bundle = intent.getExtras();
        String phoneNr= bundle.getString("incoming_number");

        Log.v(TAG, "phoneNr: "+phoneNr);
        mContext=context;
        database = new MyDatabaseHandler(mContext);
    }

    /* Custom PhoneStateListener <div style="display: none"></div>*/
    public class CustomPhoneStateListener  extends PhoneStateListener {

        boolean auth = true;
        private static final String TAG = "StateListener";

        @Override
        public void onCallStateChanged(int state, String incomingNumber){

            if(incomingNumber!=null&&incomingNumber.length()>0) incoming_nr=incomingNumber;

            switch(state){
                case TelephonyManager.CALL_STATE_RINGING:
                    Log.d(TAG, "CALL_STATE_RINGING");
                    prev_state=state;
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    Log.d(TAG, "CALL_STATE_OFFHOOK");
                    prev_state=state;
                    break;
                case TelephonyManager.CALL_STATE_IDLE:
                    Log.d(TAG, "CALL_STATE_IDLE==>" + incoming_nr);

                    getLastCallDetail();
//                    if((prev_state==TelephonyManager.CALL_STATE_OFFHOOK)){
//                        prev_state=state;
//                        //Answered Call which is ended
//
//                    }else if((prev_state==TelephonyManager.CALL_STATE_RINGING)){
//                        prev_state=state;
//                        //Rejected or Missed call
//                        getLastCallDetail();
//                    }
                    break;

            }
        }
    }

    private void getLastCallDetail(){


        long currentTime = getTodayTimestamp();
        String timestap = String.valueOf(currentTime);
        Uri contacts = CallLog.Calls.CONTENT_URI;
        Cursor managedCursor = mContext.getContentResolver().query(contacts, null, CallLog.Calls.DATE + ">= ?", new String[]{timestap}, null);

        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);

        if(managedCursor.getCount()>0) {

            while(managedCursor.moveToNext()) {
                String phNumber = managedCursor.getString(number);
                String callType = managedCursor.getString(type);
                String callDate = managedCursor.getString(date);
                //Date callDayTime = new Date(Long.valueOf(callDate));
                String callDuration = managedCursor.getString(duration);

                Log.i("call type****", "call type" + callType);
                String dir = null;
                int dircode = Integer.parseInt(callType);
                switch (dircode) {
                    case CallLog.Calls.OUTGOING_TYPE:
                        dir = "DIALED";
                        break;

                    case CallLog.Calls.INCOMING_TYPE:
                        dir = "RECEIVED";
                        break;

                    case CallLog.Calls.MISSED_TYPE:
                        dir = "MISSED";
                        break;
                    default:
                        dir = CallLog.Calls.TYPE;
                        break;
                }


                CallDetail callDetail = new CallDetail(phNumber, dir, callDate, callDuration);
                Log.i("Call log Telephone", "phNumber = " + phNumber);
                Log.i("Call log Telephone", "dir " + dir);
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss aaa");
                Log.i("Call log Telephone", "callDate" + formatter.format(new Date(Long.parseLong(callDate))));
                // SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ss");
                // Log.i("Call log Telephone", "callDate" + formatter2.format(new Date(Integer.parseInt(callDuration) * 1000L)));
                database.addCallDetail(callDetail);

            }
            if(database!=null){
                database.close();
            }

//            SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy");
//            Log.i("Call log", " " + format1.format(TimeUnit.MILLISECONDS.toSeconds(Long.valueOf(callDate)) * 1000L));
//            Log.i("Call log", " " + Long.valueOf(callDuration));
        }

    }


    public static long getTodayTimestamp() {
        Calendar c1 = Calendar.getInstance();
        c1.setTime(new Date());

        Calendar c2 = Calendar.getInstance();
        c2.set(Calendar.YEAR, c1.get(Calendar.YEAR));
        c2.set(Calendar.MONTH, c1.get(Calendar.MONTH));
        c2.set(Calendar.DAY_OF_MONTH, c1.get(Calendar.DAY_OF_MONTH));
        c2.set(Calendar.HOUR_OF_DAY, 0);
        c2.set(Calendar.MINUTE, 0);
        c2.set(Calendar.SECOND, 0);

        return c2.getTimeInMillis();
    }
}