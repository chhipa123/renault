package com.renaultcalltraker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.renaultcalltraker.Util.NetworkDetector;

public class AlarmReceiverForLocation extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent k2) {
        // TODO Auto-generated method stub
       // Toast.makeText(context, "Alarm received!", Toast.LENGTH_LONG).show();

        if (new NetworkDetector(context).isConnectingToInternet()) {
            new SendMyCallLog(context);
            new SendMyLocation(context, true);
        }
    }

}
