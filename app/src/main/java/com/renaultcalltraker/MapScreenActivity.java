package com.renaultcalltraker;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.health.android.libprogressbar.CustomProgress;
import com.renaultcalltraker.Util.NetworkDetector;
import com.renaultcalltraker.Util.ServerConnection;
import com.renaultcalltraker.constant.Constant;
import com.rets.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MapScreenActivity extends Activity {


    // Google Map
    private GoogleMap googleMap;
    // latitude and longitude
    double latitude = 0;
    double longitude = 0;
    String userName = "",contact = "",email= "",time="";
    MarkerOptions marker;
    String uniq_id="",user_id="",auth_key="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_screen);

        latitude =  Double.parseDouble(getIntent().getExtras().getString("latitude"));
        longitude =  Double.parseDouble(getIntent().getExtras().getString("longitude"));

        userName =  getIntent().getExtras().getString("username");
        contact =  getIntent().getExtras().getString("contact");
        email  =  getIntent().getExtras().getString("email");
        time  =  getIntent().getExtras().getString("time");
        uniq_id  =  getIntent().getExtras().getString("uniq_id");
        user_id  =  getIntent().getExtras().getString("user_id");
        auth_key  =  getIntent().getExtras().getString("auth_key");
        try {
            // Loading map
            initilizeMap();

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (new NetworkDetector(MapScreenActivity.this).isConnectingToInternet()) {
            new GetLatLongAsyncTask(MapScreenActivity.this, Constant.URL+"getLatLong", setJSONRequest()).execute("");

        } else {
            Toast.makeText(MapScreenActivity.this, Constant.NETWORK_ERROR_MESSAGE, Toast.LENGTH_LONG).show();
        }

        ImageButton refersh = (ImageButton)findViewById(R.id.refersh);
        refersh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (new NetworkDetector(MapScreenActivity.this).isConnectingToInternet()) {
                    new GetLatLongAsyncTask(MapScreenActivity.this, Constant.URL+"getLatLong", setJSONRequest()).execute("");

                } else {
                    Toast.makeText(MapScreenActivity.this, Constant.NETWORK_ERROR_MESSAGE, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /**
     * function to load map. If map is not created it will create it for you
     * */
    private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.map)).getMap();

            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }

    }

    private void initMarker(){
        // create marker
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss aaa");
        String timeString = formatter.format(new Date(Integer.parseInt(time) * 1000L));
        marker = new MarkerOptions().position(new LatLng(latitude, longitude))
                .title(userName)
                .snippet("Last updated time: " + timeString);
        marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        // adding marker
        googleMap.addMarker(marker);



        CameraPosition cameraPosition = new CameraPosition.Builder().target(
                new LatLng(latitude, longitude)).zoom(15).build();

        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        // Changing map type
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        // Enable / Disable zooming controls
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        // Enable / Disable my location button
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        // Enable / Disable Compass icon
        googleMap.getUiSettings().setCompassEnabled(true);
        // Enable / Disable Rotate gesture
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
    }



    @Override
    protected void onResume() {
        super.onResume();
        initilizeMap();
    }


    private JSONObject setJSONRequest() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uniq_id", uniq_id);
            jsonObject.put("user_id", user_id);
            jsonObject.put("auth_key", auth_key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


    class GetLatLongAsyncTask extends AsyncTask<String, Void, String> {
        private String TAG = "GetUserAsyncTask";
        private CustomProgress customProgress;
        private String url;
        private Context context;
        private JSONObject object;

        public GetLatLongAsyncTask(Context context, String url,JSONObject object) {
            this.url = url;
            this.context = context;
            this.object = object;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            customProgress = CustomProgress.show(context, "", false, false, null);
        }

        @Override
        protected String doInBackground(String... params) {
            String res;
            try {
                res = new ServerConnection().makePostRequest(url, object);
            } catch (Exception e) {
                res = null;
                e.printStackTrace();
            }

            return res;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            customProgress.dismiss();
            Log.i(TAG, "URL :Response  " + result);
            if (result != null) {
                parseServerResponse(result);
            } else {
                Toast.makeText(MapScreenActivity.this, Constant.NETWORK_FAIL_MSG, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void parseServerResponse(String result) {

        Log.e("parseServerResponse", "Got server response " + result);
        if (result != null) {
            String status;
            try {
                JSONObject jsonObject = new JSONObject(result);
                status = jsonObject.getString("status");
                if (status.equalsIgnoreCase("Success")) {

                    latitude =  Double.parseDouble(jsonObject.getString("lat"));
                    longitude =  Double.parseDouble(jsonObject.getString("long"));
                    time = jsonObject.getString("update_time");
                    googleMap.clear();
                    initMarker();
//                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss aaa");
//                    String timeString = formatter.format(new Date(Long.parseLong(time) * 1000L));
//                    marker = new MarkerOptions().position(new LatLng(latitude, longitude))
//                            .title(userName)
//                            .snippet("Last updated time: " + timeString);;
//                    marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
//                    // adding marker
//                    googleMap.addMarker(marker);


                } else if (status.equalsIgnoreCase("Failed.")) {
                    Toast.makeText(MapScreenActivity.this, jsonObject.getString("msg"), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(MapScreenActivity.this, Constant.NETWORK_FAIL_MSG, Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                Log.i("parse", "JSONException while parsing response :" + e);
            }
        }
    }



}
