package com.renaultcalltraker;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.renaultcalltraker.Util.UserPreference;
import com.renaultcalltraker.constant.Constant;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author Nilanchala
 *         <p/>
 *         Broadcast reciever, starts when the device gets starts.
 *         Start your repeating alarm here.
 */
public class DeviceBootReceiver extends BroadcastReceiver {

    UserPreference session;
    Context  context;
    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;
        session = new UserPreference(context);

//        if (session.isLoginDone()) {
//
//        }
        /* Setting the alarm here */
        //else {
            Log.i("devicebootreceiver", "bootup");
            setAlarmForLtLng();
            //setAlarmCallDetail();
//            if (new NetworkDetector(context).isConnectingToInternet()) {
//
//                new SendMyLocation(context,true);
//            }

      //  }

    }



    private void setAlarmForLtLng() {

        Date date = new Date();   // given date
        Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
        calendar.setTime(date);   // assigns calendar to given date


        AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(context, AlarmReceiverForLocation.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmIntent.setData((Uri.parse("custom://" + System.currentTimeMillis())));
        alarmManager.cancel(pendingIntent);
        Calendar alarmStartTime = Calendar.getInstance();
        Calendar now = Calendar.getInstance();
        alarmStartTime.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY));
        alarmStartTime.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE)+1);
        alarmStartTime.set(Calendar.SECOND, 0);
        if (now.after(alarmStartTime)) {
            alarmStartTime.add(Calendar.DATE, 1);
        }
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmStartTime.getTimeInMillis(), Constant.ALARM_INTERVAL, pendingIntent);

    }

    private void setAlarmCallDetail() {
        Date date = new Date();   // given date
        Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
        calendar.setTime(date);   // assigns calendar to given date

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(context, AlarmReceiver.class); // AlarmReceiver1 = broadcast receiver
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmIntent.setData((Uri.parse("custom://" + System.currentTimeMillis())));
        alarmManager.cancel(pendingIntent);
        Calendar alarmStartTime = Calendar.getInstance();
        Calendar now = Calendar.getInstance();
        alarmStartTime.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY)+1);
        alarmStartTime.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE)+5);
        alarmStartTime.set(Calendar.SECOND, 0);
        if (now.after(alarmStartTime)) {
            alarmStartTime.add(Calendar.DATE, 1);
        }
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmStartTime.getTimeInMillis(), Constant.ALARM_INTERVAL_FOR_CALL, pendingIntent);
    }

}
